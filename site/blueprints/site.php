<?php if(!defined('KIRBY')) exit ?>

title: Site
pages: true
fields:
  logo:
    label: Logo
    type:  text
  subtitle:
    label: Subtitle
    type:  text
  bgcolor:
    label: Background Color
    type:  color
    default: 22272a
    width: 1/2
  accolor:
    label: Accent Color
    type:  color
    default: e85151
    width: 1/2
  line-a:
    type: line
  line-b:
    type: line
  title:
    label: Title
    type:  text
  author:
    label: Author
    type:  text
  description:
    label: Description
    type:  textarea
  keywords:
    label: Keywords
    type:  tags
  copyright:
    label: Copyright
    type:  textarea