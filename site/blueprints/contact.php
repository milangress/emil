<?php if(!defined('KIRBY')) exit ?>

title: Contact
pages: true
files: true
fields:
  title:
    label: Title left
    type:  text
    width: 1/2
  titleright:
    label: Title rIght
    type:  text
    width: 1/2
  textleft:
    label: Text links
    type:  textarea
    width: 1/2
  textright:
    label: Text rechts
    type:  textarea
    width: 1/2
