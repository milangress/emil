<?php
/**
 * milangress Support Widget
 * Displays a widget in the kirby panel's dashboard showing support options.
 *
 * @author Thomas Lebeau <thomas@lebeau.io>
 * @version 1.0.0
 */

if (c::get('milangress.widget')) {
  return array(
    'title' => ($title = c::get('milangress.widget.title'))? $title : 'Milan Gress theme support',
    'html'  => function() {
      $defaults = array(

        array(
          "label" => "Website",
          "text" => "milangress.de",
          "href" => "http://milangress.de"
        )
      );
      $data = ($links = c::get('milangress.widget.links'))? $links : $defaults;
      return tpl::load(__DIR__ . DS . 'template.php', $data );
    }
  );
}
return false;
