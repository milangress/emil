<?php snippet('html-header');?>

<?php snippet('header', array('page' => $page));?>

	<div id="content">

		<?php
		foreach($pages->visible() as $section) {
 		 snippet($section->uid(), array('data' => $section));
		}
		?>
 
	</div>

<?php snippet('html-footer');?>
