<?php if(!defined('KIRBY')) exit ?>

username: milan
email: admin@milangress.de
password: >
  $2a$10$y5zVIwIoKfR0JE4oiNUgP.gAl/401UVqTrGwYxYPOEEStOKD17wVS
language: en
role: admin
history:
  - footer
  - about-me
  - contact
  - testimonials
  - projects/project-c
