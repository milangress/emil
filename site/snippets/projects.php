<section class="row">
			<div class="col-full">
				<h2><?php echo $data->title()->html()?></h2>
				<p>
					<?php echo $data->text()->kirbytext()?>
				</p>
			</div>
		</section>

		<section class="row">
			<div class="photo-grid">
			<?php foreach($data->children()->visible() as $project ): ?>
				<a href="<?php echo $project->images()->first()->url() ?>" rel="lightbox" class="col-<?=$project->colums_mg()?>"><img src="<?php echo $project->images()->first()->url() ?>" alt="<?php echo $project->title()->html() ?>"></a>
			<?php endforeach ?>
			</div>
		</section>