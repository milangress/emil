<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="<?= $site->description() ?>">
	<meta name="keywords" content="<?= $site->keywords() ?>">

	<link rel="shortcut icon" href="img/favicon.ico"> 
	<?php echo css('assets/css/vendor/fluidbox.min.css'); ?>
	<?php echo css('assets/fonts/stylesheet.css'); ?>
	<?php echo css('assets/css/main.css'); ?>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	

	<title><?php echo $site->title();?></title>

</head>
<body>