<?php
$imagesvg = $page->images()->filterBy('extension', 'svg'); 
$imagepng = $page->images()->filterBy('extension', 'png'); 
?>


<header>

		<div id="logo-container">
		<?php echo $page->images()->url() ?>
		<?php echo $imagepng ?>
			
			<div id="logo">
			<?php
			if (!$site->logo()->isEmpty()): ?>
				<a href=\"/\"><?= $page->logo() ?></a>
			<?php else: ?>
				<?php if(!empty($imagesvg)): ?>
						<object data="<?php echo $imagesvg->url(); ?>" alt="<?php echo $site->title() ?> Logo">
						<img src="<?php echo $imagepng->url(); ?>" alt="<?php echo $site->title() ?> Logo" />
					</object>
				<?php else: ?>
				<?php endif ?>
			<?php endif;?>
			</div>


			<div id="subtitle"><?= $page->subtitle() ?></div>

		</div>	

		
		

	</header>