<?php

kirbytext::$tags['fa'] = array(
  'attr' => array(
    'text'
  ),
  'html' => function($tag) {

    $article = $tag->attr('fa');

    return '<i class="fa fa-' . $article . '"></i>';

  }
);